
[TOC]

Pour motoriser la Charrette, deux solutions ont été développées:

 * Une solution simple et résiliente basée sur l'utilisation d'un accélérateur au guidon de l'usager.
 * Une solution plus complexe qui autorise la remorque à piloter elle-même son moteur avec un capteur de force.


# Généralités sur les vélos électriques

Un vélo électrique fonctionne grâce à

 * Un vélo
 * Une batterie
 * Un moteur
 * Un contrôleur
 * Des accessoires de commande qui peuvent être:
    * Une gâchette d'accélération
    * Un frein
    * Un capteur de pédalage
    * Le Cycle Analyst

Le contrôleur agit comme chef d'orchestre, il est relié à la batterie, au moteur et aux accessoires de commande.
En fonction de ces derniers et des signaux envoyés, le contrôleur transforme le courant de la batterie en signaux de puissance triphasé, le moteur tourne alors.


## La gâchette ou accélérateur

La gâchette d'accélération est un potentiomètre, une résistance variable.
Ainsi elle a 3 fils:

 * VCC ou 5V
 * Un signal échelonné entre 0 et 5V
 * GND

Si la gâchette n'est pas activée, on envoie 0V au contrôleur, la roue ne tourne pas.
Si la gâchette est actionnée au maximum, on envoie 5V au contrôleur, la roue tourne avec toute la puissance possible
Et entre les deux, le contrôleur envoie plus ou moins de puissance dans le moteur.

_Ceci n'est qu'à moitié vrai, en pratique les contrôleurs acceptent une plage de tension spécifique pour le signal de la gâchette et peuvent se mettre en défaut en dehors de cette plage_

## Le frein
Le système de frein est basé sur un interrupteur, il y a donc 2 fils:

 * Signal
 * GND

La poignée de frein est équipée d'un interrupteur. Dès que le levier de frein est actionné, le circuit est fermé, le signal est mis à la masse, le contrôleur arrête d'accélérer et active le frein moteur s'il en est capable. Auquel cas la batterie se recharge.

## Le "Cycle Analyst"

Il s'agit d'un ordinateur de bord qui se branche sur le contrôleur et qui permet d'en régler et lire les paramètres. Il vous sera possible de connaître l'état de la batterie, de fixer le niveau de régénération lors du freinage et d'accéder à la température moteur.

Ce dispositif a été créé par [Grintech](https://ebikes.ca), il n'est pas obligatoire mais si vous partez pour un voyage au long cours, il est utile d'en avoir un.

Se référer au site du fabricant pour plus d'informations en anglais ou au revendeur [Ozo](https://ozo-electric.com)



## Schéma de principe


![Schéma de principe](img/principe_fonctionnement_assistance_electrique.png)

Pour davantage d'information sur le fonctionnement du vélo électrique, je vous invite à lire [cette page](https://ebikes.ca/getting-started/ebikes-parts-explained.html) en anglais. Le site [ebikes.ca](https://ebikes.ca) est une ressource fiable à recommander.

## Point législation

L'utilisation d'un accélérateur est autorisé sur les vélos en dessous de 6km/h pour les premiers mètres. La puissance nominale développée ne peut excéder 250W pour les vélos à assistance électrique. Au delà il s'agit d'un vélomoteur qui doit être immatriculé et assuré.

La remorque à vélo est considérée comme un accessoire de cycle, la réglementation en vigueur définie dans la norme NF EN 15918/IN2 impose simplement que la remorque ne soit pas motrice. Ainsi tant qu'elle ne pousse pas le vélo on peut mettre un moteur développant 1500W sans problème. Tout tient dans le fait que la remorque ne soit pas motrice donc.

\clearpage
# Connectiques

## Connecteur de signaux

Pour les connectiques de signaux, la gamme [JST-SM](https://www.jst-mfg.com/product/pdf/eng/eSM.pdf) apporte satisfaction. Ces connecteurs sont déjà utilisés dans le monde des vélos électriques.

| Nom connecteur      | Référence fabricant |
| ------------------- | ------------------- |
| JST 3 voies mâle    | SMR-03V-BC          |
| JST 3 voies femelle | SMP-03V-BC          |
| JST 4 voies mâle    | SMR-04V-B           |
| JST 4 voies femelle | SMP-04V-BC          |
| JST 5 voies mâle    | SMR-05V-B           |
| JST 5 voies femelle | SMP-05V-BC          |


| Nom contacteur à sertir | Référence fabricant |
| ----------------------- | ------------------- |
| JST contacteur mâle     | SYM-001T-P0.6       |
| JST contacteur femelle  | SHF-001T-0.8BS      |

Se référer à ce document pour toute question sur ces connecteurs [JST-SM](https://www.jst-mfg.com/product/pdf/eng/eSM.pdf)

## Connecteur de puissance

Pour les connectiques de puissance (phase moteur, batterie), la gamme [Anderson](https://www.andersonpower.com/content/dam/app/ecommerce/product-pdfs/DS-PP1545.pdf) est satisfaisante.
Nous nous approvisionnons chez RS components.
Les connecteurs Amass XT-60 et MR-60 représentent une alternative un peu moins coûteuse.

_Attention aux malfaçons! L'achat de connecteurs de mauvaise qualité ou de copies peut engendrer une détérioration rapide et un échauffement dangereux aux points de contact_

| Composant           | Référence fabricant |
| ------------------- | ------------------- |
| Connecteur Anderson | Powerpole 1327      |
| Pin Anderson 30A    | Powerpole 1331      |
| Amass 2 phases      | XT-60               |
| Amass 3 phases      | MR-60               |

Cf [ce document](https://www.ebikes.ca/documents/GrinConnectorGuide.pdf) en anglais de ebikes.ca pour plus d'information.

Cette [page](https://ebikes.ca/learn/connectors.html) venant toujours du Canada est intéressante aussi

## Autres connecteurs

Pour les connectiques panneaux de signaux, nous utilisons actuellement les connecteurs [Neutrik NC-MX6](https://www.thomann.de/fr/neutrik_nc6mx.htm) utilisés dans le monde du spectacle.

Pour les connectiques panneaux de puissance, nous utilisons les connecteurs [Varytech PM](https://www.thomann.de/fr/varytec_real_pm.htm) aussi utilisés dans l'univers de la régie technique.

### Conventions de câblage

#### JST

  1. GND
  2. signal
  3. signal
  4. ...
  100. Vcc

#### Anderson

  Masse à gauche, V+ à droite

#### Neutrik NC-MX6

<!-- ![NC6mx](img/NC6mx_pinout.png) -->

| Pin      | Couleur | Convention | Panneau de contrôle | Effet hall | Capteur de force |
| -------- | ------- | ---------- | ------------------- | ---------- | ---------------- |
| 1        | Noir    | GND        | GND                 | GND        | E-               |
| 2        | Blanc   | Signal     | diode               | N/A        | A-               |
| 3        | Jaune   | Signal     | Allumage contrôleur | Jaune      | N/A              |
| 4        | Vert    | Signal     | Mode                | Vert       | A+               |
| 5        | Bleu    | Signal     | Frein               | Bleu       | N/A              |
| 6        | Rouge   | Vcc        | Vcc                 | Vcc        | E+               |
| Blindage | GND     | GND        | GND                 | GND        | GND              |

# Câblerie

## Signaux

Pour les signaux, il conviendra d'utiliser du câble 6 brins blindé.

Du câble ethernet souple fera l'affaire sinon nous utilisons [cette référence de chez RS](https://fr.rs-online.com/web/p/cables-de-controle/2369269).

## Puissance

Câble électrique souple en 3G2.5 accessible dans tous les magasins de bricolage.

\clearpage
# Branchement du contrôleur sur le moteur et la batterie

## Composants

| Composant                          | Description                                                                         | Qté | Proposition                                                                                                            |
| ---------------------------------- | ----------------------------------------------------------------------------------- | -------- | ---------------------------------------------------------------------------------------------------------------------- |
| Contrôleur                         | 36 ou 48V puissance selon usage, 1000W recommandé                                   | 1        | Grintech concepteur et fabricant canadien, Ozo revendeur français                                                      |
| Moteur roue avant                  | Moyeu moteur de roue avant – 1000W recommandé                                       | 1        | Grintech concepteur et fabricant canadien, Ozo revendeur français, Crystalyte, récupération d’un moteur 250W classique |
| Batterie                           | 36 ou 48V, capacité selon usage 720Wh minimum recommandé                            | 1        | Battery Empire                                                                                                         |
| Connecteur Anderson ou Amass MR60  | Connecteur de puissance pour relier le moteur au contrôleur                         | 6        | Revendeur sur internet                                                                                                 |
| Connecteur Anderson ou Amass XT-60 | Connecteur de puissance pour relier la batterie au contrôleur                       | 4        | Revendeur sur internet                                                                                                 |
| Connecteur JST 5                   | Connecteur de signaux pour relier les capteurs effets hall du moteur au contrôleur  | 2        | Revendeur sur internet                                                                                                 |
| Connecteur JST 2                   | Connecteur de signaux pour relier le capteur de température du moteur au contrôleur | 2        | Revendeur sur internet                                                                                                 |


## Installation

Nous recommandons d'installer la batterie et le contrôleur en façade de la Charrette à l'extérieur, boulonné sur une platine. Selon l'usage il pourra convenir de tout mettre dans une boîte à l'intérieur de la remorque.

![Installation batterie et contrôleur](img/controleur_batterie.jpg)


**Ne pas travailler sous tension quand vous changer des connecteurs ou brancher des câbles.**

 1. Brancher les câbles de phase,  de signaux hall et de température moteur
 2. Brancher la batterie au contrôleur
 3. Fixer les câbles au châssis pour éviter tout arrachage lors du roulage et des manœuvres.

 \clearpage

# Motorisation de la charrette avec accélérateur

La solution la plus simple est d'installer une gâchette d'accélérateur comme celles équipant les trop-nombreuses trottinettes électriques.

Les charrettes étant équipées d'un freinage inertiel, il y a des pièces en mouvement lorsque la remorque va plus vite que le vélo. En installant un contacteur à ce niveau là, le signal de la gâchette est coupé sitôt que la remorque est en avance sur le vélo.



Voici un schéma de principe:

![Schéma de principe de l'assistance manuelle avec contacteur](img/principe_assistance_gachette.png)

![Contacteur sur la platine à fixer au frein à inertie](img/contacteur_platine.jpg)

## Composants

| Composant              | Description                  | Quantité   | Autre                                                                                                                                                                                                                                                                                                                                     |
| ---------------------- | ---------------------------- | ---------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Contacteur             | Omron D2SW-P01L1H            | 1          | [sur RS]([https://fr.rs-online.com/web/p/microrupteurs/6822171](https://fr.rs-online.com/web/p/microrupteurs/6822171))                                                                                                                                                                                                                  |
| Platine de fixation    | Découpe laser                | 1          | [DXF](./découpe_laser/platine_microrupteur.dxf) – [Freecad](./découpe_laser/platine_microrupteur.Fcstd)                                                                                                                                                                                                                             |
| Gâchette               | Type gâchette de trottinette | 1          | [ebike-solutions.com]([https://www.ebike-solutions.com/en/shop/pedelec-e-bike-parts/power-throttles/ebs-daumengas-universal-halteschelle-zum-oeffnen.html?number=aeth12-thv-u](https://www.ebike-solutions.com/en/shop/pedelec-e-bike-parts/power-throttles/ebs-daumengas-universal-halteschelle-zum-oeffnen.html?number=aeth12-thv-u)) |
| Levier de frein        | Avec contacteur              | 1          | Revendeur sur internet                                                                                                                                                                                                                                                                                                                    |
| Câble                  | 6 brins blindé               | 3 mètre    | sur RS                                                                                                                                                                                                                                                                                                                                    |
| JST 3 mâle             | connecteur pour gachette     | 2 minimum  | sur RS                                                                                                                                                                                                                                                                                                                                    |
| JST 3 femelle          | connecteur pour gâchette     | 2 minimum  | sur RS                                                                                                                                                                                                                                                                                                                                    |
| JST 4 mâle             | connecteur pour frein        | 2 minimum  | sur RS                                                                                                                                                                                                                                                                                                                                    |
| JST 4 femelle          | connecteur pour frein        | 2 minimum  | sur RS                                                                                                                                                                                                                                                                                                                                    |
| JST 5 mâle             | connecteur pour rallonge     | 3 minimum  | sur RS                                                                                                                                                                                                                                                                                                                                    |
| JST 5 femelle          | connecteur pour rallonge     | 3 minimum  | sur RS                                                                                                                                                                                                                                                                                                                                    |
| contacteur JST mâle    |                              | 25 minimum | sur RS                                                                                                                                                                                                                                                                                                                                    |
| contacteur JST femelle |                              | 25 minimum | sur RS                                                                                                                                                                                                                                                                                                                                    |







## Câblerie

| Composant                         | Description                                     | Quantité | Longueur            |
| --------------------------------- | ----------------------------------------------- | -------- | ------------------- |
| Câble du contrôleur au contacteur | JST 3 mâle & JST 4 mâle &harr; JST 5 femelle    | 1        | 60cm                |
| Câblage du contacteur             | JST 5 mâle &harr; JST 5 femelle                 | 1        | 20cm                |
| Câble le long du timon            | JST 5 mâle &harr; JST 5 femelle                 | 1        | 100cm               |
| Câble le long du vélo             | JST 5 mâle &harr; JST 3 femelle & JST 4 femelle | 1        | en fonction du vélo |


![Câble du contrôleur au contacteur](img/controleur_contacteur.jpg)

![Câble du timon](img/cable_timon.jpg)

![Câble vélo](img/cable_velo.jpg)

![Gâchette](img/gachette.jpg)

![Frein](img/frein_contacteur.jpg)


 \clearpage

## Notice de câblage

### Connecteurs

Convention de câblage:

Les connecteurs liés à la gâchette sont des JST 3 voies:

| Pin | Utilisation | Couleur |
| --- | ----------- | ------- |
| 1   | GND         | Noir    |
| 2   | Signal      | Bleu    |
| 3   | Vcc         | Rouge   |


Les connecteurs liés au frein sont des JST 4 voies.
Seules deux pins sont utilisées, il s'agit là d'un simple circuit qui se ferme pour activer le frein:

| Pin | Utilisation | Couleur |
| --- | ----------- | ------- |
| 1   | N/A         | N/A     |
| 2   | Signal      | Jaune   |
| 3   | N/A         | N/A     |
| 4   | GND         | Vert    |


Les connecteurs liés aux rallonges sont des JST 5 voies:

| Pin | Utilisation     | Couleur |
| --- | --------------- | ------- |
| 1   | GND             | Noir    |
| 2   | Signal gâchette | Bleu    |
| 3   | Vcc             | Rouge   |
| 4   | Signal frein    | Jaune   |
| 5   | GND frein       | Vert    |


### Câblage et montage du microrupteur

1. Couper 15cm de câble
2. Couper 2 fils électrique de 10cm
3. Dénuder les fils électriques
4. Sertir les contacts sur les fils (mâle d'un côté, femelle de l'autre)
5. Mettre de la gaine thermodynamique comme représenté sur les photos ci-dessous
6. Insérer les contacts dans les connecteurs
7. Souder les deux fils électriques libres sur le contacteur sur les sorties Normalement Ouvert
8. Chauffer les gaines
9. Le contacteur est prêt


![Préparation du contacteur](img/contacteur_avant.jpg)

![Contacteur préparé](img/contacteur.jpg)

Pour la platine, il faut souder deux vis M2.3 de 10mm à travers les deux trous prévus:

![Platine microrupteur avec vis](img/platine_vis.jpg)

![Platine microrupteur soudée](img/platine_soudee.jpg)


![Contacteur fermé](img/contacteur_ferme.jpg)

![Contacteur ouvert](img/contacteur_ouvert.jpg)

## Roulage et conseil d'utilisation

Une fois votre vélo équipé d'un accélérateur et du levier de frein avec contacteur, il vous suffit d'atteler la remorque et de les raccorder avec les rallonges le long du timon au contrôleur.
Allumer la batterie et votre contrôleur si besoin puis il vous suffit d'actionner votre accélérateur pour soulager la charge.
Il vous faudra faire preuve de délicatesse pour ne pas subir des saccades lors d'une accélération trop forte.
Faite quelques aller-retours sur une zone dégagée avant de vous engager sur la route et gardez à l'esprit que vous prenez plus de 3 mètres en longueur.

\clearpage

# Motorisation de la charrette avec un système autonome

Sera évoquée ci-dessous l'électronique de l'assistance électrique utilisant un capteur de force. Il s'agit du Charduino, contraction de Charrette et Arduino.


Pour rappel voici un schéma de principe:

![Schéma de principe](img/principe_fonctionnement_assistance_autonome.png)


## Généralités

Le charduino est le circuit imprimé qui gère l'électronique de la charrette.
Les composants principaux du système sont:

 * Un arduino nano
    * Il traite toutes les données venant des différents capteurs et envoie les ordres au contrôleur
 * Un contrôleur de moteur électrique
    * Il active le moteur ou le frein moteur en fonction du signal de la gâchette et du signal de frein
    * Ces deux signaux sont générés par l'arduino, en fonction des capteurs
 * Une batterie 36 ou 48V
    * Plomb ou lithium
    * 48V pour avoir plus de couple
 * Un moteur électrique de vélo
    * 1200W pour assurer de déplacer 300kg
 * Un capteur de force
    * Des jauges de déformation qui quantifient la compression ou la traction de la barre d'attelage.
    * Cette information est envoyée à l'arduino
 * Les capteur effet hall du moteur
    * Capteurs intégrés dans le moteur.
    * Ils renseignent sur la vitesse et le sens de rotation de la roue.
 * Un boîtier de contrôle
    * Pour renseigner l'utilisateur sur l'état du système
    * Pour sélectionner différents mode de fonctionnement (activation, frein, mode piéton)



Ce système n'a rien d'intelligent, on parlera d'une assistance autonome. Il s'agit d'asservir le signal envoyé au contrôleur par rapport à l'information renvoyée par le capteur de force en utilisant un algorithme PID. Cette solution a l'avantage de limiter le nombre de câble mais elle ajoute un degré de complexité au système, une boîte noire qui en cas de panne peut bloquer l'usager.
Dans une démarche de résilience et low-tech, nous recommandons d'utiliser la solution avec un accélérateur et un contacteur.

Pour rentrer dans le cadre légal il est nécessaire que la remorque reste un accessoire de cycle et ne soit pas motrice. Le système repose donc sur l'idée suivante, mettre en équilibre la charrette par rapport au mouvement du vélo en s'appuyant sur le capteur de force et la vitesse de rotation de la roue. L'affinage de l'algorithme d'asservissement et le tarage du capteur de force avant démarrage sont primordiaux pour avoir une assistance satisfaisante.



## Charduino

La carte principale intrègre différents systèmes:

 * Transformateur DC-DC pour alimenter le circuit en 5V depuis la batterie en 36, 48 ou 52V
 * Pont diviseur de tension pour mesurer la tension de la batterie
 * Un amplificateur opérationnel pour mesurer le courant de décharge de la batterie
 * Une carte HX711 qui amplifie le signal du capteur de force
 * Un relais pour piloter le contrôleur
 * Une carte Arduino pour lire les informations précédemment listées et envoyer des signaux au contrôleur

![Carte Charduino montée](img/charduino_carte.jpg)

## Boîtier de contrôle

Un boîtier de contrôle est fixé sur la barre d'attelage.
Celui-ci permet:

 * d'activer le système
 * de sélectionner un mode d'assistance lent ou normal
 * de forcer l'utilisation du frein moteur
 * de renseigner l'utilisateur sur l'état du système

 Il s'agit là de deux interrupteurs et d'une diode de contrôle.

## Liste composants
### Charduino
| Composant | Description | Quantité | Référence |
| ----------- | ----------- | ----------- | ----------- |
| Charduino | Circuit imprimé principal de la charrette | 1 |
| Arduino Nano | Microcontroleur | 1
| HX711 | Amplificateur pour capteur de force | 1 | Site chinois |
| Relais Omron G5V-2-H1| Activation du contrôleur de moteur | 1 | K1 |
| Transistor NPN générique | activation du frein moteur | 1 | Q1 |
| Résistance 5mOhm | Mesure de courant | 1 | R8 |
| Résistance 1kOhm | Pont diviseur de tension | 1 | R5 |
| Résistance 470 kOhm| Amplificateur opérationnel | 2 | R7 - R10|
| Résistance 10 kOhm | Amplificateur opérationnel et pont diviseur de tension | 3 | R6 - R9 - R4 |
| Résistance 2,2kOhm | Tirage par le bas et filtre RC | 3 | R1 - R2 - R3 |
| LM7525HVT| Transformateur 5V | 1 | U2 |
| Condensateur 100µF 63V | Transformateur 5V | 1 | C1 |
| Condensateur 330µF 25V | Transformateur 5V | 1 | C2 |
| Diode 1N5819 | Transformateur 5V | 1 | D1 |
| Bobine 330µH 2A | Transformateur 5V | 1 | L1 |
| MPC602 | Amplificateur opérationnel | 1 | U3 |
| Condensateur 22µF 25V | Filtre RC| 1 | C3 |
| Capteur de force 500kg | Capteur de force | 1 | [GoTronic](https://www.gotronic.fr/art-capteur-de-force-500-kg-czl301-20800.htm) |

### Boîtier de contrôle

| Composant | Description | Quantité | Référence |
| ----------- | ----------- | ----------- | ----------- |
| Carte | Circuit imprimé du boîtier de contrôle | 1 | |
| Diode | Affichage utilisateur | 1 | [RS](https://fr.rs-online.com/web/p/indicateurs-pour-montage-panneau/0205325) |
| Interrupteur | Interface homme/machine | 2 |[RS](https://fr.rs-online.com/web/p/interrupteurs-a-bascule/7932529) & [RS](https://fr.rs-online.com/web/p/interrupteurs-a-bascule/7932531) |
| Boîte aluminium | Protection | 1 | |
| Presse-étoupe| étanchéité du câble| 1 | |

![Le boîtier de contrôle](img/boitier_de_controle.jpg)

Interrupteur principal:

 * I: un mode doux qui conviendra dans la majorité des cas.
 * 0: système éteint, le contrôleur est éteint, le moteur ne pourra pas s'enclencher
 * II: mode plus nerveux à utiliser pour des utilisateurs expérimentés.

L'interrupteur du frein n'a que deux positions:

 * 0, le frein s'activera au gré des calculs de l'Arduino
 * 1, le frein régénératif est activé et maintenu, à utiliser en descente

### Câblerie

| Composant             | Description                         | Quantité                   | Autre  |
| --------------------- | ----------------------------------- | -------------------------- | ------ |
| JST 3 mâle            | connecteur pour gachette            | 1                          | sur RS |
| JST 4 mâle            | connecteur pour frein               | 1                          | sur RS |
| JST 5 mâle et femelle | connecteur pour capteur de force    | 1                          | sur RS |
| JST 6 mâle et femelle | connecteur pour boîtier de contrôle | 1                          | sur RS |
| Connecteur Anderson   | circuit de puissance                | 4                          | sur RS |
| Câble 6 brins blindé  | pour faire les rallonges            | fonction de l’installation | sur RS |


## Installation

 * Le capteur de force est à boulonner sur le timon en l'intercalant avant l'attache de tige de selle.
 * Le boîtier de contrôle est à boulonner sur la poignée du timon.
 * Le Charduino doit être installé dans une boîte à l'abri de l'humidité, utiliser des presse-étoupes pour assurer l'étanchéité des câbles.
 * Connecter le capteur de force, le boîtier de contrôle, le connecteur de la gâchette, celui du frein et celui de l'allumage du contrôleur
 * Connecter la batterie

 ![Capteur de force](img/capteur_de_force.jpg)

\clearpage

## Programmation

Le code source du Charduino et l'ensemble des librairies est présent dans le répertoire [Informatique](../Informatique).

Utiliser l'IDE Arduino pour charger le logiciel.

## Utilisation

Lors de chaque attelage sur un nouveau vélo il conviendra de tarer le capteur de force

### Tarage

Le tarage consiste à définir la position d'équilibre pour le capteur de force:

 * Atteler le vélo sur un sol plat
 * Allumer le système avec l'interrupteur principal du boîtier de contrôle, la diode de contrôle est allumée fixe
 * Basculer 6 ou 7 fois de suite l'interrupteur du frein puis le laisser sur I, la diode de contrôle clignote à 1Hz
 * Attendre 3 secondes
 * Basculer l'interrupteur du frein sur 0
 * Tarage terminé

### Roulage

Avant de démarrer, basculez l'interrupteur du frein sur 0, le principal sur I ou II puis commencez à pédaler.
L'assistance s'enclenche avec une certaine latence en fonction du mode utilisé.
Pour changer de mode il faudra s'arrêter, basculer l'interrupteur puis repartir.
Lors d'un arrêt long, activez le frein ou alors coupez le système pour éviter tout démarrage si vous veniez à déplacer le vélo.
Gardez à l'esprit que votre vélo et sa remorque sont long d'un peu plus de 3 mètres et qu'avec votre chargement vous avez une inertie non-négligeable.
Nous vous recommandons de faire des tours sur une zone dégagée pour bien prendre en main votre Charrette et son assistance avant de vous engager sur la route.
