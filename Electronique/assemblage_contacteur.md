# Câblage et assemblage du microrupteur

1. Couper 15cm de câble à 4 brins minimum
2. Couper 2 fils électrique de 10cm
3. Dénuder les fils électriques
4. Sertir les contacts sur les fils (mâle d'un côté, femelle de l'autre)
5. Mettre de la gaine thermodynamique comme représenté sur les photos ci-dessous
5. Insérer les contacts dans les connecteurs
6. Souder les deux fils électriques libres sur le contacteur sur les sorties Normalement Ouvert
7. Chauffer les gaines
9. Le contacteur est prêt
