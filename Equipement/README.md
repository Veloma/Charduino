# Equipement vélo
<<<<<<< HEAD

[La notice de montage du timon avec le frein à inertie](./frein_a_inertie_timon.pdf)

[La documentation de montage](./manuel_montage/montage.pdf)
=======
| Composant | Description | Quantité | Autre |
| ----------- | ----------- | ----------- | ----------- |
| Moyeu |	roulement industriel axe 15mm entraxe 100mm | 2 ou 3 |  |
| Jante |	20" - Mach 1 Kargo 36 trous double paroi- oeillet | 3 |  |
| Rayons |	Rayons 13G renforcé 2.0 ou 2.3mm |  108 |  |
| Pneus | Anti crevaison Schwalbe marathon ou Continental | 3 |  |
| Disque de frein | Disque 203mm inox | 2 |  |
| Gaine de frein | 	gaine de freins |  3m |  |
| Câble de frein | 	cable inox tandem 3m | 2 |  |
| Frein avant | V-brake | 1 |  |
| Frein à main | Poignée de frein à verrouillage | 1 |  |
| Jeu de direction | 	Jeu de direction roulement intégré 44mm | 1 |  |
| Garde-boue | 	aluminium 50x3mm | 2 |  |
| Etrier de frein | Double tirage Zoom ou Avid BB5 | 2 |  |
| Feu arrière | 	Feu arrière solaire autonome | 2 |  |
| Potence | Potence ahead zoom 1’1/8 | 1 |  |
| Fond de jante | Haute pression | 3 |  |
| Roulement potence |  Roulement industriel 10x26mm | 2 |  |
| Fourche | Fourche BMX 20" 28,6mm renforcée avec V-brake | 1 |  |
>>>>>>> dc08a437b901b7766092565620ad20f51ec7a027
