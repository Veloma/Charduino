# CHARRETTE: CHARRETTE Hardi Auto-freinante Remorquable Réservable Esthétique Tractable Tenace Électrique

## Description
Charrette est une remorque à vélo à trois roues avec système  de  freinage  inertiel et un système d’assistance électrique autonome ou pilotable manuellement.
 * Pensée  pour  l’autoconstruction  et  une  diffusion large, la remorque s’attelle sur toutes les tiges de selle de vélo  ou se tire à la main.
 * Le  mécanisme  du  frein  à  inertie  automatique  protège remorque et cycliste en permettant d’augmenter la charge utile jusqu’à 300 kg.
 * Le format de chargement est prévue pour la vélologistique sur palette ou boites transport Euro.
 * Une assistance électrique est conçue pour que la charrette accompagne indépendamment le vélo en modulant sa  propre puissance.
 * D’autres éléments déployables, solaires ou fonctionnels  assurent une grande variété d’usages (restauration, culturel, social....).
 * La remorque en source libre est conçue en tubes carrés simples à provisionner et facilement soudable.

### Historique selon la Charrette
Moi, la charrette, remorque à vélo dont la conception a été commencée et est maintenue à ce jour par Véloma, je suis le fruit d'un long cheminement, d'itérations successives, d'expérimentations pour obtenir un engin roulant tractable à vélo stable et efficace.
Ce chemin a croisé celui d'amis, de personnes désireuses de donner un avis ou dans le besoin de livrer des plantes en moins d'une heure de l'autre côté d'une ville trop chargée par des véhicules vides. Sur la route, j'ai rencontré l'ADEME, au travers d'un Appel à Manifestation d'Intérêt de l'agence francilienne lancé en mai 2020. Cet AMI avait pour thème les basses-technologies, ou low-tech.

Voici quels sont les objectifs énoncés par [cet appel à projet](/pdf/AMI.pdf)

>Ouvert, ascendant et promouvant des approches **résolument systémiques de l'innovation**, le présent AMI poursuit plusieurs objectifs **dans une perspective d’autonomie, d’autosuffisance, d’adaptabilité, de transformabilité et de résilience territoriales** :
>
>* Questionner l’organisation socioéconomique et territoriale des activités humaines à l’aune de l’échelle bien proportionnée pour la mise en œuvre d’une démarche « low- tech » : modèles organisationnels, modèles d’affaires, synergies et solidarités interterritoriales, etc. ;
* Interroger les besoins en ressources au regard des limites physiques (finitude des ressources, etc.), technico-économiques (phénomène de saturation des capacités des systèmes techniques, taux de retour énergétique décroissants des ressources énergétiques, endettements, rendements théoriques maximaux, rendements décroissants des systèmes complexes, etc.) et humaines (résistances au changement de comportement) : sobriété, écologie de la demande, etc. ;
* Étudier, dans toutes leurs dimensions (technologique, fiscale, législative, réglementaire, normative, etc.), les conditions d’émergence ainsi que les freins et les leviers au (re)déploiement de systèmes sociotechniques et économiques plus intensifs en emplois et plus sobres en consommation de ressources. Il s’agira ici de s’interroger sur la quantité et la qualité du travail humain et de caractériser ce que seraient des métiers et des emplois « low-tech » ainsi que les trajectoires de reconversion professionnelle associées dans les prochaines décennies à l’aune de la notion d’« esclave énergétique » et dans un contexte d’accès aux ressources (énergétiques notamment) de plus en plus limité. En particulier, seront-ils plus ou moins salariés, plus ou moins rémunérés, plus ou moins partagés, plus ou moins réflexifs et scientifiquement éclairés, plus ou moins manuels, plus ou moins physiques ?
* Contribuer à remettre de la lucidité et de redonner du sens à l’innovation et à réinventer des métiers et emplois inspirants compatibles avec la réalité physique d’un monde fini par la transformation des récits collectifs aujourd’hui dominés par le paradigme de l’inflation technologique et de l’accumulation matérielle.


Ainsi au printemps 2020 alors que j'étais en prototypage dans l'atelier de Véloma, mes trois pneus se sont gonflés, on m'a accroché derrière un vélo et je me suis élancée dans les rues séquano-dyonisiennes pour livrer denrées et semis.
Pendant ce temps, Véloma a envoyé un dossier à l'ADEME et est devenue, comme 9 autres structures, lauréate de cet appel à manifestation d'intérêt.

J'ai été propulsée sous les feux d'une petite communauté de bricoleurs, de créateurs, de rêveurs qui oeuvrent pour créer un autre monde, un monde où l'on pourrait aller faire ses courses avec son vélo, un monde où l'on pourrait m'emprunter ou me louer dans une maison de quartier, un monde où l'on pourrait télécharger mes plans sur internet pour les donner au soudeur du coin et qu'une soeur naisse ailleurs, un monde où l'on pourrait m'augmenter, me transfigurer, m'adapter à des usages non prévus lors de ma conception.

Ce monde décrit à l'instant, c'est celui vers lequel me porte Véloma avec l'aide de l'ADEME, du réseau mis à disposition et des copains dont tu peux faire partie. Je suis sur cette route depuis novembre 2020 et jusqu'à octobre 2022.

## Libre, auto-constructible et réparable
Les concepteurs initiaux de la remorque croient en un monde inspiré du logiciel libre, un monde qui n'a pas peur de partager ses connaissances pour les voir être appropriées par d'autres, améliorées, modifiées ou détournées.
Ainsi l'intégralité des plans de la charrette seront mis à disposition sur ce site sous licence CERN-OHL-W v2. Le code sera partagé sous licence CC-BY-SA.

## La remorque
La remorque est un engin roulant à trois roues qui s'attele sur la tige de selle d'un vélo.
Elle peut aussi être tractée par un humain voire un animal de traie.
La structure et les roues ont été dimensionnées pour supporter 300kg.
La surface utile de chargement est de 1705x650mm, les ridelles font 330mm de haut, la garde au sol est de 360mm.

### Structure
Les tubes de la structure sont soudés ensemble pour assurer rigidité et longue vie au châssis.
Des plans de découpe laser des tubes existent mais la découpe à la main de chaque tube est possible.

Nous travaillons avec de l'acier inox 304 et 316 mais il est possible d'utiliser n'importe quel acier de qualité, un passage à la peinture sera à considérer.

Pour la soudure, nous avons des postes TIG et MIG dans notre atelier mais lors des formations, la soudure à l'arc avec baguette inox est utilisée.

![Châssis de la remorque](img/charrette-65x165.jpg)

Une réflexion est menée pour la fabrication de fourche 20":


![Fourche 20"](img/fourche20.jpg)


### Equipement vélo
La charrette est un équipement de cycles et à ce titre est équipée comme un vélo:

* Jantes kargo;
* Rayons inox renforcés;
* Moyeux renforcés;
* Garde-boue en aluminium;
* Axes de roues diamètre 15 en acier plein et faits chez un tourneur;
* Freinage à câble;
* Etrier de frein à disque BB5;
* Jeu de direction interne;
* Potence courte.

Tous ces équipements sont au maximum sourcés en France ou en Europe.
Une attention toute particulière a été portée aux roues que nous assemblons nous-mêmes afin qu'une charge de 300kg soit supportées dynamiquement entre les trois roues et axes.


### Système de freinage inertiel
Un mouvement relatif entre la barre d'attelage et la remorque est autorisé par un roulement linéaire dans le sens de la marche.
Un ressort limite les mouvements et tend à maintenir la remorque dans une position au repos.
En cas de freinage du pilote, la remorque vient s'appuyer sur la barre d'attelage et tire deux câbles qui freinent les roues arrières.
Il s'agit là d'un frein à inertie qui est primordial pour la tenue de route et la sécurité du chargement.


### Frein de stationnement
A l'arrêt, pour sécuriser la Charrette, il est possible de maintenir le frein à inertie actionné avec un crochet. Le frein à inertie devient alors frein de stationnement.


![Frein à inertie et de stationnement](img/frein-a-inertie.jpg)


### Tenue de route
Nous n'avons pas encore réussi à retourner la charrette dans un virage.

## Assistance électrique

Là où les vélos sont limités à 250W, les remorques à vélo n'observent pas cette limite tant qu'elles ne sont pas motrices, entendez qu'elles ne poussent pas le vélo.
Le système d'assistance électrique met donc la remorque en équilibre derrière le cycliste et active même le freinage moteur en cas de besoin.
Nous préconisons un moteur de 1200W et une batterie de 48V afin d'avoir assez de puissance pour déplacer jusqu'à 300kg, il faudra tout de même appuyer sur les pédales.

A minima il faudra donc s'équiper:
* d'un moteur moyeu 1000/1500W monté dans une roue 20"
* d'un contrôleur de moteur 48V 25A
* d'une batterie de 48V 20Ah

Deux types d'assistance ont été développés:
 - Une assistance "intelligente" qui détecte le retard de la remorque par rapport au cycliste et assiste en fonction.
 - Une assistance manuelle qui est pilotée par une gachette au guidon du cycliste.


### Assistance autonome

En équipant la barre d'attelage de jauge de contrainte, il est possible de piloter l'assistance électrique avec un arduino.
Ce système est mis sous tension par un simple interrupteur, ensuite sitôt que la remorque détecte qu'elle est en retard par rapport au vélo, elle actionnera son moteur jusqu'à rattraper le cycliste et dans le cas contraire où elle ira plus vite, la remorque ralentira voire activera son frein moteur.
Cette assistance, plus simple à l'utilisation car il n'y a aucune modification à apporter au vélo-tracteur, est aussi plus compliquée à mettre en oeuvre soit-même et dépend de composants électroniques supplémentaires à approviosionner et à assembler.

![Schéma de principe](img/principe_assistance_capteur.png)

### Assistance manuelle

Les systèmes d'assistance électrique standard peuvent fonctionner avec une gachette d'accélération. L'exemple en est des trottinettes électriques.
Il est possible de piloter la Charrette avec une gachette en tirant un câble jusqu'au guidon.
Pour rester mettre de la remorque, et aussi rester dans le cadre de la loi française, il est impératif que le vélo reste tracteur et ne soit pas propulsé par la Charrette.
En équipant le frein à inertie d'un contacteur, il est possible de couper le signal envoyé au contrôleur. Sitôt que la remorque vient appuyer sur le vélo, le frein à inertie s'enclenche, le contacteur aussi ouvrant le circuit et coupant l'assistance. L'ajout d'un circuit RC entre le contrôleur et le contacteur permet de lisser les accélérations et déccélérations.
Cette solution est plus facile à mettre en oeuvre pour le constructeur mais moins comode pour l'usager qui doit préparer son vélo avant de partir.
![Schéma de principe](img/principe_assistance_gachette.png)


## Système de partage

Un système informatique, en cours de développement, permettra de mutualiser l'utilisation d'une charrette ou de n'importe quel vélo, pour étendre les imaginaires de toute un quartier. Le code s'appuiera sur celui d'[Open Source Bikeshare](https://github.com/cyklokoalicia/OpenSourceBikeShare).

Ce système de partage à vocation à fonctionner sur un ordinateur type Raspberry Pi et pourra tout aussi bien gérer la mutualisation d'une charrette que de vélos en partage.

## Modulaire

La charrette peut être modifiée facilement et gagner de nouvelles fonctionnalités.

### Charrette-Pikip
Ainsi il est possible d'ajouter des panneaux solaires par exemple et une enceinte [Pikip](https://www.pikip-solarspeakers.com) pour transformer la charrette en remorque Boum-Boum, un engin de célébrations massives.


![Boumboum](img/boumboum.jpg)

### Panneaux déployables
Les tubes verticaux sont pensés comme chandeliers pour accueillir d'autres tubes et ainsi autoriser le pivotement de panneaux.
Un exemple d'une charrette utilisée par l'association Déenbulles, une ludothèque itinérante.

![Ludothèque](img/ludothèque.jpg)

![DJ Scène](img/DJ-scène.jpg)

### Examples d'utilisations:

Davantage de photos sur [Charrette.bike](https://charrette.bike)

![Piano](img/piano.jpg)

## Coût de revient

Le temps passé à chercher et acheter les composants n'est pas pris en compte.

| Partie | Description | Total |
| ----------- | -----------  | ----------- |
| Structure | Tubes, boulonnnerie, consommable soudure, ...| ???? |
| Equipement vélo | Roues, câbles, gaines, ... | ???? |
| Electronique Charduino| Composants, câbles, connecteurs | ???? |
| Electronique assistance| Moteur, contrôleur, batterie | ???? |
| Total| Matériel | ???? |


## Temps d'assemblage

Le temps passé à chercher, dessiner et/ou acheter les composants n'est pas pris en compte. On considère que les différentes techniques nécessaires sont déjà acquises à la personne qui assemble.

| Partie | Description | Total |
| ----------- | -----------  | ----------- |
| Structure | Mise en place, sous contrainte, soudure, nettoyage | 2 jour |
| Equipement vélo | Rayonnage des deux roues arrière, installation des composants, réglage des freins | 1,5 jours |
| Electronique Charduino| Soudure des composants, connectiques | 1 jour |
| Electronique assistance| Préparation connectique, branchement | 0,5 jour |
| Total| Temps passé pour une charrette | 5 jours |
